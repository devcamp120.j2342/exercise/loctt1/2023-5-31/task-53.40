import models.Circle;
import models.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle =  new Circle(2.5);
        System.out.println("Circle: ");
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());
        System.out.println(circle.toString());
        System.out.println("------------------------------------");

        Rectangle rectangle = new Rectangle(2, 4);
        System.out.println("Rectangle: ");
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.toString());
        System.out.println("------------------------------------");
    }
}
